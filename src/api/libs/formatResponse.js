const formatResponse = ({data, status}) => {
  return {
    data,
    status,
  }
}

export {
  formatResponse
};