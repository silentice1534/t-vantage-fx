import restful from "@/api/libs/restful";

const request = restful({
  baseURL: "https://api.coingecko.com/api/",
  version: "v3",
  useAuth: false,
  timeout: 3000,
});

export default request;
