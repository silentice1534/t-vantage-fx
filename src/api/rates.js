import request from "@/api";

export const ratesListAPI = async (payload) => {
  const res = await request({
    method: "get",
    url: "/exchange_rates",
    data: payload,
  });
  return res.data;
};
