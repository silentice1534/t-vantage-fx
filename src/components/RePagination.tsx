import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";
import React, { useState } from "react";
import { BsArrowLeftShort, BsArrowRightShort } from "react-icons/bs";
import ReSelect from "@/components/ReSelect";

interface IPaginationTotal extends IFunctionComponent {
  size?: any;
  pager?: any;
  handlePageIndex: (targetPage: any) => void;
  handlePageSize: (pageSize: any) => void;
}

const RePagination: React.FunctionComponent<IPaginationTotal> = ({
  pager = {
    pageIndex: 1,
    totalPage: 1,
    pageSize: 10,
  },
  className,
  handlePageIndex,
  handlePageSize,
}) => {
  const [firstPage, setFirstPage] = useState(1);

  // 渲染頁數
  const pageList = () => {
    // 如果小於6 直接返回
    if (pager.totalPage <= 6) {
      const pageList = [];
      for (let i = 1; i <= pager.totalPage; i += 1) {
        pageList.push(i);
      }
      return pageList;
    }

    let pageList = [
      pager.pageIndex - 2,
      pager.pageIndex - 1,
      pager.pageIndex,
      pager.pageIndex + 1,
      pager.pageIndex + 2,
    ];

    pageList = pageList.filter(
      (ele) => ele >= firstPage && ele <= pager.totalPage
    );

    // 補1
    if (pager.pageIndex - 2 - firstPage === 1) {
      pageList.splice(0, 0, 1);
    }

    // 補... 和 1
    if (pager.pageIndex - 2 - firstPage > 1) {
      pageList.splice(0, 0, "⋯");
      pageList.splice(0, 0, 1);
    }

    // 補最大頁數
    if (pager.totalPage - (pager.pageIndex + 2) >= 1) {
      pageList.push(pager.totalPage);
    }

    // 補最大頁數...
    if (pager.totalPage - (pager.pageIndex + 2) > 1) {
      pageList.splice(-1, 0, "⋯");
    }

    return pageList;
  };

  const handleJumpPage = (operate: any, page: any) => {
    // 唯一的string是刪節號，直接返回
    if (typeof page === "string") {
      return;
    }

    let targetPage = 0;
    if (operate === "prev") {
      targetPage = pager.pageIndex - 1;
    }

    if (operate === "next") {
      targetPage = pager.pageIndex + 1;
    }

    if (operate === "jump") {
      targetPage = page;
    }

    handlePageIndex(targetPage);
  };

  const pageSize = [
    {
      label: 5,
      value: 5,
    },
    {
      label: 10,
      value: 10,
    },
    {
      label: 20,
      value: 20,
    },
  ];

  const renderPage = pageList().map((item: any, index: any) => {
    return (
      <li
        className={`re-pager-item ${
          typeof item !== "number" && "more-sign-bg"
        } ${
          Number(pager.pageIndex) === Number(item) && "re-pager-item--active"
        }`}
        key={index}
        onClick={() => handleJumpPage("jump", item)}
      >
        <span
          className={`page-number ${typeof item !== "number" && "more-sign"} ${
            Number(pager.pageIndex) === Number(item) && "page-index"
          }`}
        >
          {item}
        </span>
      </li>
    );
  });

  return (
    <div className={`re-pagination ${className}`}>
      {pager.totalPage >= 1 && (
        <ul className="re-pager-list">
          {pager.pageIndex !== 1 && (
            <li
              className="re-pager-item pager-jump-bg"
              onClick={() => handleJumpPage("prev", null)}
            >
              <div className="r-arrow">
                <BsArrowLeftShort className="r-arrow__icon" />
              </div>
            </li>
          )}

          {renderPage}

          {pager.pageIndex !== pager.totalPage && pager.totalPage >= 1 && (
            <li
              className="re-pager-item pager-jump-bg"
              onClick={() => handleJumpPage("next", null)}
            >
              <div className="r-arrow">
                <BsArrowRightShort className="r-arrow__icon" />
              </div>
            </li>
          )}
        </ul>
      )}

      <ReSelect
        options={pageSize}
        handlePageSize={handlePageSize}
        pager={pager}
      />
    </div>
  );
};

const StyledComp = styled(RePagination)`
  ${({ theme: { mixin } }) => mixin("flex")("flex-end")}
  width: 100%;
  margin-top: 30px;

  .hide {
    display: none;
  }

  .re-pager-list {
    display: inline-flex;
    margin-right: 30px;
  }

  .re-pager-item {
    ${({ theme: { mixin } }) => mixin("boxPadding")("0")}
    display: inline-block;
    position: relative;
    width: 36px;
    height: 36px;
    background-color: transparent;
    cursor: pointer;

    &--active {
      border-bottom: 1px solid ${({ theme }) => theme.main};
    }
  }

  .page-number {
    ${({ theme }) => theme.mixin("font")(theme.text.basic, "12px")}
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .more-sign-bg {
    background-color: transparent;
    cursor: pointer;
  }

  .page-index {
    color: ${({ theme: { global } }) => global("black")};
  }

  .pager-jump-bg {
    background-color: transparent;
  }

  .page-jump {
    display: inline-block;
    width: 20px;
    vertical-align: bottom;
  }

  .r-arrow {
    width: 36px;
    height: 36px;
    transition: 0.3s;

    &__icon {
      ${({ theme: { mixin } }) => mixin("position")("center")}
      width: 18px;

      &::before,
      &::after {
        content: "";
        display: inline-block;
        position: absolute;
        top: 50%;
        width: 6px;
        height: 2px;
        border-radius: 1px;
        background-color: ${({ theme }) => theme.text.baisc};
        transform: translateY(-50%);
      }

      &::before {
        left: -2px;
        transform: rotate(45deg) translateY(-50%);
      }

      &::after {
        right: -2px;
        transform: rotate(-45deg) translateY(-50%);
      }
    }
  }
`;

export default StyledComp;
