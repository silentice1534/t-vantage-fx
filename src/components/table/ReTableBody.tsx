import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";
import { Fragment } from "react";
import ReCheckbox from "@/components/ReCheckbox";

interface IReTableBody extends IFunctionComponent {
  tableData: any[];
  columnConfig: any[];
}

const ReTableBody: React.FunctionComponent<IReTableBody> = ({
  className,
  children,
  tableData,
  columnConfig,
}) => {
  const redenrTds = (data: any) => {
    const renderContent = (item: any) => {
      if (item.renderType === "checkbox") return <ReCheckbox />;
      if (item.render) return item.render(data[item.prop], data);
      if (item.formatter) {
        return item.formatter(data[item.prop], data);
      }
      return data[item.prop];
    };

    return columnConfig.map((col) => {
      return (
        <td className="re-table-body__tr__td" key={col.prop}>
          <div className="re-table-body__tr__td__content">
            {renderContent(col)}
          </div>
        </td>
      );
    });
  };

  const renderTrs = (rowData: any) => {
    return rowData.map((data: any) => {
      return (
        <Fragment key={data.id}>
          <tr className="re-table-body__tr">{redenrTds(data)}</tr>
          {data.children && renderTrs(data.children)}
        </Fragment>
      );
    });
  };

  return (
    <tbody className={`re-table-body ${className}`}>
      {renderTrs(tableData)}
    </tbody>
  );
};

const StyledComp = styled(ReTableBody)`
  .re-table-body {
    &__tr {
      border-top: 1px solid ${({ theme }) => theme.table.trDivide};

      &__td {
        &__content {
          ${({ theme }) => theme.mixin("font")(theme.text.basic, "14px")}
          ${({ theme: { mixin } }) => mixin("boxPadding")("20px 10px")}
          display: inline-block;
        }
      }
    }
  }
`;

export default StyledComp;
