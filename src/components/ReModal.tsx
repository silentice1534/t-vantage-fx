import React from "react";
import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";

interface IModalComp extends IFunctionComponent {
  modalSwitch: boolean;
  onClick: () => void;
  title: string;
}

const Modal: React.FunctionComponent<IModalComp> = ({
  className,
  children,
  onClick,
  title,
}) => {
  const handleClick = () => {
    onClick();
  };

  const stopBubble = (e: React.MouseEvent) => {
    e.stopPropagation();
  };

  return (
    <div className={`comp-modal ${className}`} onClick={handleClick}>
      <div className="comp-modal__content" onClick={stopBubble}>
        <div className="comp-modal__content__box">
          <p className="comp-modal__content__box__title">{title}</p>
          <div className="comp-modal__content__box__children">{children}</div>
        </div>
      </div>
    </div>
  );
};

const styledComp = styled(Modal)<IModalComp>`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 500;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  opacity: ${({ modalSwitch }) => (modalSwitch ? 1 : 0)};

  transition: 0.2s;

  .comp-modal {
    &__content {
      ${({ theme: { mixin } }) => mixin("boxPadding")("15px")}
      ${({ theme: { mixin } }) => mixin("position")("center")}
      width: 90%;
      max-width: 450px;
      height: auto;
      overflow: hidden;
      transform: translate(-50%, -50%);
      transition: 0.4s;

      &__box {
        ${({ theme: { mixin } }) => mixin("boxPadding")("20px")}
        display: inline-block;
        width: 100%;
        height: 100%;
        border-radius: 4px;
        background-color: #fafafa;
        overflow: hidden;
        box-shadow: rgba(0, 0, 0, 0.2) 0 4px 12px;

        &__children {
          position: relative;
          z-index: 2;
          height: 100%;
        }

        &__title {
          ${({ theme }) =>
            theme.mixin("font")(theme.global("error"), "24px", "700")}
          z-index: 1;
          margin-bottom: 30px;
        }
      }
    }
  }
`;

export default styledComp;
