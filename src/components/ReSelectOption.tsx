import styled from "styled-components";
import { IFunctionComponent } from "@/interface/IFunctionComponent";

interface IReSelectOption extends IFunctionComponent {
  option: { [key: string]: string };
  handlePageSize: (pageSize: any) => void;
}

const ReSelectOption: React.FunctionComponent<IReSelectOption> = ({
  className,
  option,
  handlePageSize,
}) => {
  return (
    <li
      className={`re-select-option ${className}`}
      onClick={() => handlePageSize(option.value)}
    >
      <span className="re-select-option__label">{option.label}</span>
    </li>
  );
};

const StyledComp = styled(ReSelectOption)<IReSelectOption>`
  ${({ theme: { mixin } }) => mixin("boxPadding")("10px")};
  ${({ theme }) => theme.mixin("font")(theme.text.basic, "14px")}
  display: inline-block;
  width: 100%;
  color: ${({ theme }) => theme.global(theme.text.basic)};
  cursor: pointer;
  background-color: rgba(${({ theme: { global } }) => global("main")}, 0.1);

  &:hover {
    background-color: rgba(12, 31, 80, 0.1);
  }
`;

export default StyledComp;
