import ReCheckbox from "@/components/ReCheckbox";
import Home from "@/views/Home";

export const ratesListConfig = [
  {
    label: "",
    renderType: "checkbox",
    prop: "select",
    width: 50,
  },
  {
    label: "Name",
    prop: "name",
    width: 200,
  },
  {
    label: "Type",
    prop: "type",
    width: 100,
  },

  {
    label: "Unit",
    prop: "unit",
    width: 100,
  },
  {
    label: "Value",
    prop: "value",
    width: 100,
  },
];
