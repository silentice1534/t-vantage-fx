interface Itheme {
  [key: string]: {
    [key: string]: any;
  };
}

const theme: Itheme = {
  mixin: {
    flex() {
      return (jc = "flex-start", ai = "center", fc = "row") => ({
        display: "flex",
        "justify-content": jc,
        "align-items": ai,
        "flex-direction": fc,
      });
    },
    font() {
      return (c = "#000", fs = "16px", fw = "400") => ({
        color: c,
        fontSize: fs,
        fontWeight: fw,
      });
    },
    boxPadding() {
      return (p: "0") => ({
        boxSizing: "border-box",
        padding: p,
      });
    },
    position() {
      return (type = "center", tb: "0", lr: "0") => {
        if (type === "center") {
          return {
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          };
        }

        const positionCss: { [key: string]: any } = {
          tl() {
            return { position: "absolute", top: tb, left: lr };
          },
          tr() {
            return { position: "absolute", top: tb, right: lr };
          },
          bl() {
            return { position: "absolute", bottom: tb, left: lr };
          },
          br() {
            return {
              position: "absolute",
              bottom: tb,
              right: lr,
            };
          },
        };

        return positionCss[type]();
      };
    },

    cusRadius() {
      return (tl = "0", tr = "0", br = "0", bl = "0") => ({
        borderTopLeftRadius: tl,
        borderTopRightRadius: tr,
        borderBottomRightRadius: br,
        borderBottomLeftRadius: bl,
      });
    },
    circle() {
      return (wh = "10px", r = "50%") => ({
        width: wh,
        height: wh,
        borderRadius: r,
      });
    },
  },
  global: {
    main: "#0c1f50",
    mainDeep: "#0c1f50",
    mainDeep2: "#172e81",
    mainLight: "#22D3EE",
    mainGradient: "linear-gradient(40deg, #0c1f50 0%, #172e81 100%)",
    assist: "#f44336",
    switchBg: "#1e1b1f",
    black: "#333",
    white: "#fff",
    navText: "#fff",
    navHover: "#d32f2f",
    navActive: "#d32f2f",
    navDecorate: "#999",
    error: "#F44336",
    errorRGB: "244, 67, 54",
    bg: "#fafafa",
  },
  light: {
    mode: "light",
    layoutBg: "#eceff1",
    text: {
      basic: "#424242",
    },
    table: {
      bg: "#fafafa",
      trDivide: "#ccc",
    },
    search: {
      bg: "#fafafa",
    },
    switchBg: "#270637",
    global: function (key: string) {
      return theme.global[key];
    },
    mixin: function (func: string) {
      return theme.mixin[func]();
    },
  },
  dark: {
    mode: "dark",
    layoutBg: "#270637",
    text: {
      basic: "#F5F5F5",
    },
    table: {
      bg: "#455A64",
      trDivide: "#78909C",
    },
    search: {
      bg: "#455A64",
    },
    switchBg: "#270637",
    global: function (key: string) {
      return theme.global[key];
    },
    mixin(key: string) {
      return theme.mixin[key]();
    },
  },
};

const themeMode: string = localStorage.getItem("themeMode") || "light";

let usedThemeSetting: { [key: string]: any } = {};

if (themeMode === "light") {
  usedThemeSetting = theme.light;
} else {
  usedThemeSetting = theme.dark;
}

export { usedThemeSetting };
export default theme;
