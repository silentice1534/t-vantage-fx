import { Iroutes } from "@/interface/routes";
import { Route } from "react-router-dom";
import { basic, menu } from "@/router/routes";

import Home from "@/views/Home";
import RatesList from "@/views/RatesList";

const view: { [key: string]: any } = {
  Home,
  RatesList,
};

const basicRoutes = basic.map((route: Iroutes<string>) => (
  <Route
    path={route.path}
    component={view[route.component]}
    exact={route.exact}
    key={route.path}
  />
));

const menuRoutes = menu.map((route: Iroutes<string>) => (
  <Route
    path={route.path}
    component={view[route.component]}
    exact={route.exact}
    key={route.path}
  />
));

const router = [...basicRoutes, ...menuRoutes];

export default router;
