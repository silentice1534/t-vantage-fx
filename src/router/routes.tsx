import { Iroutes } from "@/interface/routes";

export const basic: Iroutes<string>[] = [
  {
    path: "/",
    component: "Home",
    exact: true,
    name: "首頁",
  },
];

export const menu: Iroutes<string>[] = [
  {
    path: "/rates-list",
    component: "RatesList",
    name: "RatesList",
    exact: true,
  },
  {
    path: "/page-two",
    component: "PageTwo",
    name: "PageTwo",
    exact: true,
  },
  {
    path: "/page-three",
    component: "PageThree",
    name: "PageThree",
    exact: true,
  },
];
