import styled from "styled-components";
import { useState, useEffect } from "react";

import { IFunctionComponent } from "@/interface/IFunctionComponent";
import { ratesListConfig } from "@/config/ratesList";
import { ratesListAPI } from "@/api/rates";
import { BsFilter } from "react-icons/bs";

import ReRow from "@/components/ReRow";
import ReTable from "@/components/table";
import ReTitle from "@/components/ReTitle";
import RePagination from "@/components/RePagination";
import ReModal from "@/components/ReModal";

type TrateData = {
  id?: string;
  name: string;
  unit: string;
  type: string;
  value: number;
};

interface IRatesList extends IFunctionComponent {}

const RatesList: React.FunctionComponent<IRatesList> = ({ className }) => {
  const [tableData, setTableData] = useState<TrateData[]>([]);
  const [storeData, setStoreData] = useState<TrateData[]>([]);
  const [drawerSwitch, setDrawerSwitch] = useState(false);
  const [pager, setPager] = useState({
    pageIndex: 1,
    totalPage: 6,
    pageSize: 10,
  });

  const handlePageIndex = (pageIndex: any) => {
    setPager((prevState) => {
      return {
        ...prevState,
        pageIndex,
      };
    });
  };

  const handlePageSize = (pageSize: any) => {
    const totalPage = 60 % pageSize !== 0 ? 60 / pageSize + 1 : 60 / pageSize;

    setPager({
      pageIndex: 1,
      totalPage,
      pageSize,
    });
  };

  const handleTableData = () => {
    const { pageIndex, pageSize } = pager;
    const startIndex = (+pageIndex - 1) * +pageSize;
    const endIndex = +pageSize;
    const data = JSON.parse(JSON.stringify(storeData));

    setTableData(data.splice(startIndex, endIndex));
  };

  const handleDrawerSwitch = () => {
    setDrawerSwitch(!drawerSwitch);
  };

  useEffect(() => {
    const getRatesList = async () => {
      // 有網路打請求，沒有的話看 localstorage
      if (navigator.onLine) {
        try {
          const res: any = await ratesListAPI();

          if (!res) {
            throw Error();
          }

          const { rates: bcryptObj }: { rates: { [key: string]: TrateData } } =
            res;

          const bcryptKey: string[] = Object.keys(bcryptObj);

          const rateList: TrateData[] = [];
          for (let key of bcryptKey) {
            rateList.push({ id: bcryptObj[key].name, ...bcryptObj[key] });
          }

          localStorage.setItem("rateList", JSON.stringify(rateList));

          setStoreData(rateList.slice(0, 60));
        } catch (e) {
          handleDrawerSwitch();
        }
      } else {
        const rateListInStorage: string =
          localStorage.getItem("rateList") || "";

        if (rateListInStorage !== "") {
          const rateList: TrateData[] = JSON.parse(rateListInStorage);

          setStoreData(rateList.slice(0, 60));
          return;
        }

        setStoreData([]);
      }
    };
    getRatesList();
  }, []);

  useEffect(() => {
    handleTableData();
  }, [pager.pageIndex, pager.pageSize, storeData]);

  return (
    <div className={`rates-list ${className}`}>
      <ReRow justifyContent="space-between">
        <ReTitle>Rates</ReTitle>
        <BsFilter
          className="filter-icon"
          onClick={() => console.log("click filter!")}
        />
      </ReRow>

      <ReTable tableData={tableData} columnConfig={ratesListConfig} />

      <RePagination
        pager={pager}
        handlePageIndex={handlePageIndex}
        handlePageSize={handlePageSize}
      />

      {drawerSwitch && (
        <ReModal
          modalSwitch={drawerSwitch}
          onClick={handleDrawerSwitch}
          title="Error"
        >
          Network request failed
        </ReModal>
      )}
    </div>
  );
};

const StyledComp = styled(RatesList)<IRatesList>`
  .filter-icon {
    font-size: 20px;
    cursor: pointer;
  }
`;

export default StyledComp;
