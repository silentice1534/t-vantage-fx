import styled from 'styled-components';

const Header = styled.div`
  ${({ theme: { mixin } }) => mixin('boxPadding')('30px')}
  flex: 1;
  width: 100%;
  max-width: 1200px;
  overflow: auto;
`;

export default Header;
