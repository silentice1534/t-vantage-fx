import styled from "styled-components";
import Header from "@/layout/layout/Header";
import Views from "@/layout/layout/Views";

const ly: any = {};

ly.Layout = styled.div`
  ${({ theme: { mixin } }) => mixin("flex")("flex-start", "center", "column")}
  width: 100%;
  min-height: 100%;
  background-color: ${({ theme }) => theme.layoutBg};
`;

ly.Header = Header;
ly.Views = Views;

export default ly;
