import styled from 'styled-components';

const Header = styled.div`
  flex: none;
  width: 100%;
  height: auto;
  background: ${({ theme: { global } }) => global('mainGradient')};
`;

export default Header;
