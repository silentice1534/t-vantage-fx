import { createElement, useState, useContext } from "react";
import styled from "styled-components";
import { matchPath, useLocation, useHistory } from "react-router-dom";
import { IFunctionComponent } from "@/interface/IFunctionComponent";
import { Iroutes } from "@/interface/routes";
import { menu as menuRoutes } from "@/router/routes";
import themeSetting from "@/styles/theme";

import theme from "@/styles/theme";

interface INav extends IFunctionComponent {
  setTheme: React.Dispatch<React.SetStateAction<any>>;
  themeMode: string;
}

const Nav: React.FunctionComponent<INav> = ({ className, setTheme }) => {
  const location = useLocation();
  const history = useHistory();

  const activedNav: (path: string) => boolean = (path: string) => {
    return matchPath(path, { path: location.pathname })?.isExact ? true : false;
  };

  const toPage = (path: string) => {
    history.push(path);
  };

  const toHome = () => {
    history.push("/");
  };

  const navItem = menuRoutes.map((ele: Iroutes<string>) => {
    return (
      <li className="nav-list__item" key={ele.name}>
        <span
          className={`nav-list__item__text ${
            activedNav(ele.path) ? "nav-list__item__text--active" : ""
          }`}
          onClick={() => toPage(ele.path)}
        >
          {ele.name}
        </span>
      </li>
    );
  });

  return (
    <div className={className}>
      <div className="nav-logo" onClick={toHome}>
        logo
      </div>
      <ul className="nav-list">{navItem}</ul>
    </div>
  );
};

const styledComp = styled(Nav)`
  ${({ theme }) => theme.mixin("flex")("center")}
  ${({ theme }) => theme.mixin("boxPadding")("10px 30px")}
  width: 100%;

  .nav-logo {
    ${({ theme }) => theme.mixin("flex")("center", "center")}
    flex: none;
    width: 80px;
    height: 30px;
    border: 1px solid ${({ theme }) => theme.global("navText")};
    cursor: pointer;
    ${({ theme }) =>
      theme.mixin("font")(theme.global("navText"), "14px", "400")}
  }

  .nav-list {
    flex: 1;

    &__item {
      ${({ theme }) => theme.mixin("boxPadding")("0 15px")}
      ${({ theme }) =>
        theme.mixin("font")(theme.global("navText"), "14px", "400")}
      display: inline-block;
      vertical-align: middle;

      &:hover {
        font-weight: 700;
        transition: 0.4s;
      }

      &__text {
        cursor: pointer;

        &--active {
          font-weight: 700;
          position: relative;

          &::before {
            content: "";
            ${({ theme: { mixin } }) => mixin("position")("tl", "100%", "0")};
            display: inline-block;
            width: 100%;
            height: 2px;
            background-color: ${({ theme }) => theme.global("assist")}
        }
      }
    }
  }

  .nav-function {
    &__list {
      ${({ theme: { mixin } }) => mixin("flex")()};

      &__item {
        display: inline-flex;
        align-items: center;
        position: relative;
        margin-left: 15px;

        &:first-child {
          margin-left: 0;
        }

        &--divide {
          margin-left: 30px;

          &::before {
            content: "";
            display: inline-block;
            position: absolute;
            top: 50%;
            left: -15px;
            width: 2px;
            height: 20px;
            border-radius: 1px;
            background-color: ${({ theme: { global } }) =>
              global("navDecorate")};
            transform: translateY(-50%);
          }
        }

        &__icon {
          ${({ theme: { mixin, text, global } }) =>
            mixin("font")(global("white"), "20px")};
          cursor: pointer;
        }
      }
    }
  }
`;

export default styledComp;
