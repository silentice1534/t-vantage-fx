# t-vantage-fx

## node version

\>=12.0.0

## setup

- `yarn` 

## development

- `yarn dev`

## buid

- `yarn build`

## 其他

- api 假定至少收到 60 筆資料，若低於 60 筆資料，換頁部分可能會出現 bug
- checkbox 和 filter 僅有 UI，沒有撰寫任何邏輯